if ( typeof wikisoba_config.types.quiz == 'undefined' ) wikisoba_config.types.quiz = {} ;

	
wikisoba_config.types.quiz.multichoice = function ( self , page ) {
	$('#page_row').hide() ;
	$('#quiz_row').show() ;
	self.loadCSS ( 'plugins/quiz.css' ) ;

	var h = '' ;
	h += "<div class='quiz_main'>" ;
	h += "<div class='panel panel-default'>" ;
	h += "<div class='panel-heading'>" + page.title + "</div>" ;
	h += "<div class='panel-body'>" ;
	h += "<div class='lead'>" + page.question + "</div>" ;
	h += "</div>" ;

	var correct_answers = 0 ;
	if ( typeof page.checked == 'undefined' ) {
		page.checked = [] ;
		$.each ( page.answers , function ( num , answer ) {
			if ( answer.correct == 'yes' ) correct_answers++ ;
			page.checked[num] = 0 ;
		} ) ;
	} else {
		$.each ( page.answers , function ( num , answer ) {
			if ( answer.correct == 'yes' ) correct_answers++ ;
		} ) ;
	}
	
	h += "<ul class='list-group'>" ;
	$.each ( page.answers , function ( num , answer ) {
		h += "<li class='list-group-item'>" ;
		h += "<button id='answer_"+num+"' type='button' class='answer btn btn-default btn-lg'" ;
		if ( correct_answers >= 1 ) h += " data-toggle='button' aria-pressed='false' autocomplete='off'" ;
		h += ">" ;
		h += answer.label ;
		h += "</button>" ;
		h += " <div class='hint' id='hint_"+num+"'>" ;
		if ( answer.correct == 'yes' ) h += "<span class='good_answer'>✓</span>" ;
		else h += "<span class='label label-warning' style='color:black'>" + (answer.hint||'') + "</span>";
		h += "</div>" ;
		h += "</li>" ;
	} ) ;
	h += "</tbody></table>" ;

	h += "</div>" ;
	h += "</div>" ;
	
	$('#quiz_body').html(h) ;

	self.showPageNav() ;
	$('#next_page').hide() ;
	
	// Check answers, highlight problems and show hints, and offer next page
	function checkAnswers () {
		var good = 0 ;
		var bad = 0 ;
		$.each ( page.answers , function ( num , answer ) {
			if ( page.checked[num] && answer.correct == 'yes' ) good++ ;
			if ( page.checked[num] && answer.correct != 'yes' ) bad++ ;
			if ( page.checked[num] ) {
				$('#hint_'+num).css({display:'inline-block'}) ;
				if ( answer.correct == 'yes' ) $('#answer_'+num).removeClass('btn-default').addClass('btn-success') ;
				else $('#answer_'+num).removeClass('btn-default').addClass('btn-warning') ;
			}
		} ) ;
		if ( good == correct_answers && bad == 0 ) $('#next_page').show() ;
		else $('#next_page').hide() ;
	}
	
	// Click handler
	function handleClick ( num ) {
		$('div.hint').hide() ;
		$('button.answer').removeClass('btn-success').removeClass('btn-warning').addClass('btn-default') ;
		page.checked[num] = 1 - page.checked[num] ;
		
		if ( page.checked[num] && correct_answers == 1 ) {
			$.each ( page.answers , function ( k , v ) {
				if ( num == k ) return ;
				if ( page.checked[k] == 0 ) return ;
				$('#answer_'+k).button('toggle') ;
				page.checked[k] = 0 ;
			} ) ;
		}
		
		var sum = 0 ;
		$.each ( page.checked , function (k,v) { sum += v } ) ;
		if ( sum < correct_answers ) return ;
		checkAnswers() ;
	}

	$.each ( page.answers , function ( num , answer ) {
		$('#answer_'+num).click ( function () { handleClick(num) } ) ;
	} ) ;

	// Re-create previous state, if any
	$.each ( page.answers , function ( num , answer ) {
		if ( !page.checked[num] ) return ;
		$('#answer_'+num).click() ;
		handleClick(num) ;
	} ) ;
	
	self.updateHash();
}
	

wikisoba_config.types.quiz.adventure = function ( self , page ) {
	$('#page_row').hide() ;
	$('#quiz_row').show() ;
	self.loadCSS ( 'plugins/quiz.css' ) ;

	var h = '' ;
	h += "<div class='quiz_main'>" ;
	h += "<div class='panel panel-default'>" ;
	h += "<div class='panel-heading'>" + page.title + "</div>" ;
	h += "<div class='panel-body'>" ;
	h += "<div class='lead'>" + page.question + "</div>" ;
	h += "</div>" ;
	
	var idbase = 'adventure_option_' ;
	var one_line_each = !self.isTrue ( page.inline ) ;
	var option_css = "display:" + (one_line_each?"block":"inline-block") ;
	$.each ( (page.options||[]) , function ( num , o ) {
	
		h += "<div class='panel panel-primary " + idbase + num + " quiz_adventure_container' style='" + option_css + "'>" ;
		h += "<div class='panel-heading quiz_adventure_label'>" + o.label + "</div>" ;
		h += "<div class='panel-body'>" ;
	
		h += "<div class='container'>" ;
		h += "<div class='row quiz_adventure_description'>" ;

		h += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-3 quiz_adventure_thumb'>" ;
		if ( typeof o.icon != 'undefined' ) {
			h += "<img src='" + o.icon + "' />" ;
		}
		h += "</div>" ;

		h += "<div class='col-lg-10 col-md-10 col-sm-10 col-xs-9 quiz_adventure_text'>" ;
		h += o.text || '' ;
		h += "</div>" ;
		
		h += "</div>" ;
		h += "</div>" ;
		
		h += "</div>" ;
		
		h += "</div>" ;
	} ) ;

	h += "</div>" ;
	h += "</div>" ;
	
	$('#quiz_body').html(h) ;

	$.each ( (page.options||[]) , function ( num , o ) {
		$('div.'+idbase+num).click ( function () {
			var quiz = o.quiz ;
			var page = o.page ;
			self.loadQuiz ( quiz , page ) ;
		} ) ;
	} ) ;

	self.showPageNav() ;
	$('#next_page').hide() ;
	self.updateHash();
}

wikisoba_config.types.quiz.gaps_drag = function ( self , page ) {
	$('#page_row').hide() ;
	$('#quiz_row').show() ;
	self.loadCSS ( 'plugins/quiz.css' ) ;

	var pattern = /\{(\d+)\}/g ;
	var text = page.text ;
	if ( typeof page.total_questions == 'undefined' ) {
		page.attempts = 0 ;
		page.correct = 0 ;
		page.total_questions = text.match(pattern).length ;
	}
	text = text.replace ( pattern , "<span class='label label-default quiz_gaps_drag_target' num='$1'>&nbsp;&nbsp;—&nbsp;&nbsp;</span>" ) ;

	var h = '' ;
	h += "<div class='quiz_main'>" ;
	h += "<div class='panel panel-default'>" ;
	h += "<div class='panel-heading'>" + page.title + "</div>" ;
	h += "<div class='panel-body'>" ;
	h += "<div class='quiz_gaps_drag_lead'>" + text + "</div>" ;
	h += "<hr/>" ;
	
	h += "<div class='quiz_gaps_drag_lead'>" ;
	var ak = [] ;
	$.each ( page.answers , function ( k , v ) { ak.push ( k ) } ) ;
	ak = ak.sort ( function (a,b) { return( parseInt( Math.random()*10 ) %2 ) } ) ;
	$.each ( ak , function ( dummy , num ) {
		h += "<div class='label label-primary quiz_gaps_drag_item' num='"+num+"'>" ;
		h += page.answers[num].label ;
		h += "</div>" ;
	} ) ;
	h += "</div>" ;
	
	h += "</div>" ;
	h += "</div>" ;
	h += "</div>" ;
	$('#quiz_body').html(h) ;
	
	$('div.quiz_gaps_drag_item').draggable ( {
		revert: true
	} ) ;
	$('span.quiz_gaps_drag_target').droppable( {
		accept : 'div.quiz_gaps_drag_item' ,
		hoverClass : 'label-info' ,
		drop: function( event, ui ) {
			var target = $(this) ;
			var num_drop = target.attr('num') ;
			var drag = $(ui.draggable) ;
			var num_drag = drag.attr('num') ;
			page.attempts++ ;
			if ( num_drop == num_drag ) { // Correct
				page.correct++ ;
				target.html(drag.html()).removeClass('label-default').removeClass('label-info').addClass('label-success') ;
				drag.remove() ;
				if ( page.correct == page.total_questions ) $('#next_page').show() ;
				return true ;
			} else {
				return false ;
			}
		}
	} ) ;

	self.showPageNav() ;
	$('#next_page').hide() ;
	self.updateHash();
}
