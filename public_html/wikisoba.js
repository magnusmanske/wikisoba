var wikisoba = {
	quiz : {} ,
	settings : {} ,
	config : {} ,
	quizzes : {} ,
	txt : { // Active translation
		next_page : "Next page",
		prev_page : "Previous page",
		page_of : "Page $1 of $2" ,
		loading : "Loading content..."
	} ,
	
	isTrue : function ( v , default_value ) {
		if ( typeof v == 'undefined' || v == '' ) {
			if ( typeof default_value != 'undefined' ) return this.isTrue ( default_value ) ;
			else return false ;
		}
		if ( null === v ) return false ;
		if ( v.toLowerCase() == 'yes' || v == '1' || v == 1 ) return true ;
		return false ;
	} ,
	
	updateHash : function () {
		var self = this ;
		var hash = [] ;
		if ( self.settings.page != 0 ) hash.push ( 'page='+(self.settings.page+1) ) ;
		if ( typeof self.settings.json_url != 'undefined' &&
			self.config.default_quiz != self.settings.json_url ) hash.push ( 'quiz=' + encodeURIComponent ( self.settings.json_url ) ) ;

		if ( hash.length > 0 ) hash = '#'+hash.join('&') ;
		else hash = '' ;
		self.current_hash = hash ;
		window.location.hash = hash ;
	} ,
	
	loadCSS : function ( url ) {
		var self = this ;
		if ( typeof self.config.css_loaded == 'undefined' ) self.config.css_loaded = {} ;
		if ( self.config.css_loaded[url] ) return ;
		self.config.css_loaded[url] = 1 ;
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
			rel:  "stylesheet",
			type: "text/css",
			href: url
		});
	} ,
	
	getParameterByName : function ( name , def ) {
		var self = this ;
		if ( typeof def == 'undefined' ) def = '' ;
		name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regexS = "[\\#&]"+name+"=([^&#]*)";
		var regex = new RegExp( regexS );
		var results = regex.exec( window.location.href );
		if( results == null ) return def;
		else return decodeURIComponent(results[1].replace(/\+/g, " "));
	} ,

	
	reset : function () {
		var self = this ;
		self.quiz = {} ;
		self.settings = { page:0 } ;
	} ,
	
	setLoadingMessage : function ( state ) {
		var self = this ;
		if ( state ) $('#head_body').html ( "<div class='loading'>" + self.txt.loading + " <img height='12px' src='//upload.wikimedia.org/wikipedia/commons/8/85/Throbber_allbackgrounds_circledots_32.gif' /></div>" ) ;
		else $('#head_body').html ( '' ) ;
	} ,
	
	showPageNav : function () {
		var self = this ;

		if ( self.settings.page > 0 ) { // Previous page
			var h = "<button id='prev_page' type='button' class='btn btn'>" + self.txt.prev_page + "</button>" ;
			$('#foot_body_l').html ( h ) ;
			$('#prev_page').click ( function () { self.showPage ( self.settings.page-1 ) } ) ;
		}

		if ( self.settings.page+1 < self.quiz.pages.length ) { // Next page
			var h = "<button id='next_page' type='button' class='btn btn-primary'>" + self.txt.next_page + "</button>" ;
			$('#foot_body_r').html ( h ) ;
			$('#next_page').click ( function () { self.showPage ( self.settings.page+1 ) } ) ;
		}
		
		if ( self.isTrue ( self.quiz.pagenumber , 'yes' ) ) {
			var h = "<div class='page_of'>" + self.txt.page_of.replace(/\$1/g,self.settings.page+1).replace(/\$2/g,self.quiz.pages.length) + "</div>" ;
			$('#foot_body_m').html ( h ) ;
		}
	} ,
	
	showContentPage : function ( page ) {
		var self = this ;
		self.updateHash() ;
		$('#page_body').html ( page.cached_html ) ;
		if ( typeof page.postProcessHTML != 'undefined' ) page.postProcessHTML ( $('#page_body') ) ;
		self.showPageNav() ;
	} ,
	
	loadContentPageWiki : function ( page , o ) {
		var self = this ;
		var url = o.url ; // Try full URL first
		if ( typeof url == 'undefined' ) {
			url = o.api+"?format=json&action=parse&prop=text&disabletoc&disableeditsection&page="+encodeURIComponent(o.page)+"" ;
			url += "&mobileformat" ; // TESTING
			if ( typeof o.section != 'undefined' ) url += "&section="+o.section ;
		}
		self.setLoadingMessage ( true ) ;
		$.getJSON ( url+"&callback=?" , function ( d ) {
			self.setLoadingMessage ( false ) ;
			if ( typeof d.parse == 'undefined' || typeof d.parse.text == 'undefined' || typeof d.parse.text['*'] == 'undefined' ) {
				alert ( "Wiki API error for " + url ) ;
				console.log ( d ) ;
				return ;
			}
			var html = "<div class='wiki'>" + d.parse.text['*'] + "</div>" ;
			var title = o.title ;
			if ( typeof title == 'undefined' && typeof o.page != 'undefined' && o.title_as_header ) title = o.page.replace(/_/g,' ') ;
			if ( (title||'') != '' ) html = "<div class='header'>" + title + "</div>" + html ;
			page.cached_html = html ;
			page.postProcessHTML = function ( root ) {
				root.find('.error').remove() ;
				root.find('.ambox').remove() ;
				root.find('a').each ( function ( dummy , a ) {
					var href = $(a).attr('href') ;
					var m = href.match ( /^\/wiki\/(.+)$/ ) ;
					if ( m == null ) return ;
					href = url.replace(/\/[^\/]+$/,'') + "/index.php?title=" + encodeURIComponent(m[1]) ;
					$(a).attr({href:href,target:'_blank'}) ;
				} ) ;
			} ;
			o.callback() ;
//			self.showContentPage ( page ) ;
		} ) ; // TODO error handling
	} ,
	
	loadContentPage : function ( page , callback ) {
		var self = this ;
		$('#page_row').show() ;
		$('#quiz_row').hide() ;
		if ( typeof page.cached_html != 'undefined' ) { // Use cache
			callback() ;
			return ;
		}
		
		var o = {} ;
		$.each ( ((self.quiz.defaults||{}).content||{}) , function ( k , v ) { o[k] = v } ) ;
		$.each ( (page||{}) , function ( k , v ) { o[k] = v } ) ;
		o.callback = callback ;
		
		if ( o.source == 'wiki' ) {
			self.loadContentPageWiki ( page , o ) ;
		} else {
			alert ( "Unknown source type" ) ;
			console.log ( page ) ;
		}
	} ,
	
	
	precachePage : function ( pid ) {
		var self = this ;
		if ( pid < 0 || pid >= self.quiz.pages.length ) return ;
		var page = self.quiz.pages[pid] ;
		if ( page.type == 'content' ) {
			if ( typeof page.cached_html != 'undefined' ) return ;
			self.loadContentPage ( page , function () { /* Nothing, just caching */ } ) ;
		}
	} ,
	
	showPage : function ( pid ) {
		var self = this ;
		var page = self.quiz.pages[pid] ;
		self.settings.page = pid ;
		
		// Clear existing HTML
		$.each ( ['head_body','page_body','quiz_body','quiz_sidebar','foot_body','foot_body_l','foot_body_m','foot_body_r'] , function ( k , v ) {
			$('#'+v).html ( '' ) ;
		} ) ;
		
		self.precachePage ( pid-1 ) ;
		self.precachePage ( pid+1 ) ;
		
		if ( page.type == 'content' ) {
			self.loadContentPage ( page , function () { self.showContentPage ( page ) } ) ;
//		} else if ( page.type == 'quiz' ) self.loadQuizPage ( page ) ;
		} else if ( typeof wikisoba_config.types[page.type] != 'undefined' ) {
			if ( typeof wikisoba_config.types[page.type] == 'function' ) {
				wikisoba_config.types[page.type] ( self , page ) ;
			} else if ( typeof page.subtype != 'undefined' && typeof wikisoba_config.types[page.type][page.subtype] != 'undefined' ) {
				wikisoba_config.types[page.type][page.subtype] ( self , page ) ;
			} else {
				// TODO fail
			}
		} else {
			alert ( "Unknown content type on page " + (pid+1) ) ;
			console.log ( page ) ;
		}
	} ,
	
	showCurrentPage : function () {
		var self = this ;
		self.showPage ( self.settings.page ) ;
	} ,
	
	setQuiz : function ( new_quiz ) {
		var self = this ;
		self.quiz = new_quiz ;
		if ( typeof self.quiz.title != 'undefined' ) $('#top_title').html ( self.quiz.title ) ;
		else $('#top_title').html ( "<i>nameless quiz</i>" ) ;
		self.showCurrentPage() ;
	} ,
	
	loadFromProxy : function ( url , type , callback ) {
		$.get ( './proxy.php' , {
			url:url,
			type:type
		} , function ( d ) {
			callback ( d ) ;
		} ) ; // TODO error handling
	} ,
	
	loadQuiz : function ( url , page ) {
		var self = this ;
		if ( typeof url == 'undefined' ) { // Current quiz, new page
			self.showPage ( page ) ;
			return ;
		}
		self.settings.json_url = url ;
		self.settings.page = (page||0) ;
		
		if ( typeof self.quizzes[url] != 'undefined' ) { // Use cache
			self.setQuiz ( self.quizzes[url] ) ;
			return ;
		}
		
		self.loadFromProxy ( url , 'application/json' , function ( d ) {
			self.quizzes[url] = d ;
			self.setQuiz ( d ) ;
		} ) ;
	} ,
	
	init : function () {
		var self = this ;
		self.config = wikisoba_config ;
		self.reset() ;
		var json_url = self.getParameterByName ( 'quiz' ) ;
		if ( json_url != '' ) {
			var page = self.getParameterByName ( 'page' , 1 ) - 1 ;
			wikisoba.loadQuiz ( json_url , page ) ;
		} else if ( typeof self.config.default_quiz != 'undefined' ) {
			wikisoba.loadQuiz ( self.config.default_quiz , 0 ) ;
		} else {
			self.updateHash() ;
			// Load dummy page
		}
	} ,
	
	onhashchange : function(e) {
		var self = wikisoba ;
		if ( window.location.hash == self.current_hash ) return ;
		self.init() ;
	} ,
	
	this_is_the_end:'my friend, the end'
}

$(document).ready ( function () {
	if ( typeof no_auto_run != 'undefined' ) return ;
	$(window).bind( 'hashchange', wikisoba.onhashchange );
/*
	// Nice, but fires too easily!
	$$('body').swipeRight ( function (e) { history.back() } ) ;
	$$('body').swipeLeft ( function (e) {
		console.log ( e ) ;
		if ( $('#next_page').is(':visible') ) $('#next_page').click() ;
		else history.forward()
	} ) ;
*/
	wikisoba.init() ;
} ) ;